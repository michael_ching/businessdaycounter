using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessDayCounter.Tests
{
    [TestClass]
    public class BusinessDayCounterTest
    {
        [TestMethod]
        public void WeekdaysBetweenTwoDates_ShouldReturn0_WhenSecondDateIsLessThanFirstDate()
        {
            var counter = new BusinessDayCounter();
            var dates = counter.WeekdaysBetweenTwoDates(new DateTime(2013, 10, 9), new DateTime(2013, 10, 7));
            Assert.AreEqual(0, dates);
        }

        [TestMethod]
        public void WeekdaysBetweenTwoDates_ShouldReturnDates_WhenSecondDateIsGreaterThanFirstDate()
        {
            var dates = new BusinessDayCounter().WeekdaysBetweenTwoDates(new DateTime(2013, 10, 7), new DateTime(2013, 10, 9));
            Assert.AreEqual(1, dates);

            dates = new BusinessDayCounter().WeekdaysBetweenTwoDates(new DateTime(2013, 10, 7), new DateTime(2014, 1, 1));
            Assert.AreEqual(61, dates);

            dates = new BusinessDayCounter().WeekdaysBetweenTwoDates(new DateTime(2013, 10, 5), new DateTime(2013, 10, 14));
            Assert.AreEqual(5, dates);
        }

        [TestMethod]
        public void WeekdaysBetweenTwoDates_ShouldReturn0_WhenSecondDateEqualsFirstDate()
        {
            var dates = new BusinessDayCounter().WeekdaysBetweenTwoDates(new DateTime(2013, 10, 7), new DateTime(2013, 10, 9));
            Assert.AreEqual(1, dates);
        }

        [TestMethod]
        public void BusinessDaysBetweenTwoDates_ShouldReturnDates_WhenSecondDateGreaterThanFirstDate()
        {
            var dates = new BusinessDayCounter().BusinessDaysBetweenTwoDates(
                new DateTime(2013, 12, 24),
                new DateTime(2013, 12, 27),
                new List<DateTime>() { new DateTime(2013, 12, 25), new DateTime(2013, 12, 26) });

            Assert.AreEqual(0, dates);
        }

        [TestMethod]
        public void BusinessDaysInJanuary2018_ShouldSkipOnePublicHoliday()
        {
            var dates = new BusinessDayCounter().BusinessDaysBetweenTwoDates(
                new DateTime(2017, 12, 31),
                new DateTime(2018, 02, 01),
                new List<IPublicHolidayRules>() { new YearlyNonWeekendPublicHoliday(1, 26) });

            Assert.AreEqual(22, dates);
        }

        [TestMethod]
        public void DayInMonthHolidayTest()
        {
            var queensBirthdayRule = new YearlyDayInMonthPublicHoliday(6, DayOfWeek.Monday, 2);

            var queensBirthday = queensBirthdayRule.GenerateHolidayDates(new DateTime(2016, 1, 1), new DateTime(2020, 1, 1));
            var dateString = string.Join(",", queensBirthday.Select(d => d.ToString("dd-MM-yyyy")));

            Assert.AreEqual("13-06-2016,12-06-2017,11-06-2018,10-06-2019,08-06-2020", dateString);
        }

        [TestMethod]
        public void NonWeekendHolidayTest()
        {
            var christmasRule = new YearlyNonWeekendPublicHoliday(12, 25);
            var christmases = christmasRule.GenerateHolidayDates(new DateTime(2010, 1, 1), new DateTime(2018, 1, 1));
            var dateString = string.Join(",", christmases.Select(d => d.ToString("dd-MM-yyyy")));

            Assert.AreEqual(
                "27-12-2010,26-12-2011,25-12-2012,25-12-2013,25-12-2014,25-12-2015,26-12-2016,25-12-2017,25-12-2018",
                dateString);
        }

        [TestMethod]
        public void StaticPublicHolidayTest()
        {
            var anzacDayRule = new StaticYearlyPublicHoliday(4, 25);
            var anzacDays = anzacDayRule.GenerateHolidayDates(new DateTime(2010, 1, 1), new DateTime(2018, 1, 1));
            var dateString = string.Join(",", anzacDays.Select(d => d.ToString("dd-MM-yyyy")));

            Assert.AreEqual(
                "25-04-2010,25-04-2011,25-04-2012,25-04-2013,25-04-2014,25-04-2015,25-04-2016,25-04-2017,25-04-2018",
                dateString);
        }
    }
}
