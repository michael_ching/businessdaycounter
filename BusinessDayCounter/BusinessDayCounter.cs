﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessDayCounter
{
    public class BusinessDayCounter
    {
        public int WeekdaysBetweenTwoDates(DateTime firstDate, DateTime secondDate)
        {
            if (secondDate <= firstDate)
                return 0;

            return GetDatesBetweenTwoDates(firstDate, secondDate)
                .Count(d => d.DayOfWeek != DayOfWeek.Saturday && d.DayOfWeek != DayOfWeek.Sunday);
        }

        public int BusinessDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate,
            IList<DateTime> publicHolidays)
        {
            if (secondDate <= firstDate)
                return 0;

            return GetDatesBetweenTwoDates(firstDate, secondDate)
                .Except(publicHolidays)
                .Count(d => d.DayOfWeek != DayOfWeek.Saturday && d.DayOfWeek != DayOfWeek.Sunday);
        }

        public int BusinessDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate,
            IList<IPublicHolidayRules> publicHolidays)
        {
            if (secondDate <= firstDate)
                return 0;

            var holidays = publicHolidays.SelectMany(ph => ph.GenerateHolidayDates(firstDate, secondDate));

            return GetDatesBetweenTwoDates(firstDate, secondDate)
                .Except(holidays)
                .Count(d => d.DayOfWeek != DayOfWeek.Saturday && d.DayOfWeek != DayOfWeek.Sunday);
        }

        private IEnumerable<DateTime> GetDatesBetweenTwoDates(DateTime start, DateTime end)
        {
            return Enumerable.Range(1, end.Subtract(start).Days - 1)
                .Select(d => start.AddDays(d));
        }
    }
}