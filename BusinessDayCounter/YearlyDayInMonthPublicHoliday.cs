﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessDayCounter
{
    public class YearlyDayInMonthPublicHoliday : IPublicHolidayRules
    {
        private readonly int _month;
        private readonly DayOfWeek _day;
        private readonly int _weekInMonth;

        public YearlyDayInMonthPublicHoliday(int month, DayOfWeek day, int weekInMonth)
        {
            _month = month;
            _day = day;
            _weekInMonth = weekInMonth;
        }

        public IEnumerable<DateTime> GenerateHolidayDates(DateTime firstDate, DateTime secondDate)
        {
            var years = Enumerable.Range(firstDate.Year, secondDate.Year - firstDate.Year + 1);

            var holidayDates = new List<DateTime>();
            foreach (var year in years)
            {
                var date = new DateTime(year, _month, 1);
                date = date.AddDays((_weekInMonth - 1) * 7);

                while (date.DayOfWeek != _day)
                {
                    date = date.AddDays(1);
                }

                holidayDates.Add(date);
            }

            return holidayDates;
        }
    }
}