﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessDayCounter
{
    public class YearlyNonWeekendPublicHoliday : IPublicHolidayRules
    {
        private readonly int _month;
        private readonly int _day;

        public YearlyNonWeekendPublicHoliday(int month, int day)
        {
            _month = month;
            _day = day;
        }

        public IEnumerable<DateTime> GenerateHolidayDates(DateTime firstDate, DateTime secondDate)
        {
            var years = Enumerable.Range(firstDate.Year, secondDate.Year - firstDate.Year + 1);
            var dates = years.Select(y => new DateTime(y, _month, _day)).ToList();

            for (int i = 0; i < dates.Count; i++)
            {
                if (dates[i].DayOfWeek == DayOfWeek.Saturday)
                    dates[i] = dates[i].AddDays(2);
                if (dates[i].DayOfWeek == DayOfWeek.Sunday)
                    dates[i] = dates[i].AddDays(1);
            }

            return dates;
        }
    }
}