﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessDayCounter
{
    public class StaticYearlyPublicHoliday : IPublicHolidayRules
    {
        private readonly int _month;
        private readonly int _day;

        public StaticYearlyPublicHoliday(int month, int day)
        {
            _month = month;
            _day = day;
        }

        public IEnumerable<DateTime> GenerateHolidayDates(DateTime firstDate, DateTime secondDate)
        {
            var years = Enumerable.Range(firstDate.Year, secondDate.Year - firstDate.Year + 1);
            return years.Select(y => new DateTime(y, _month, _day));
        }
    }
}