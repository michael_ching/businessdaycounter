﻿using System;
using System.Collections.Generic;

namespace BusinessDayCounter
{
    public interface IPublicHolidayRules
    {
        IEnumerable<DateTime> GenerateHolidayDates(DateTime firstDate, DateTime secondDate);
    }
}